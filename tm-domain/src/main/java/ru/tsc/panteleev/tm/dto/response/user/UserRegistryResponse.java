package ru.tsc.panteleev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

@NoArgsConstructor
public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable UserDTO user) {
        super(user);
    }

}
